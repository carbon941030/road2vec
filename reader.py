# -*- coding: utf-8 -*-

import random

import tensorflow as tf

from utils import line2int_list


class Reader(object):

    def __init__(
            self,
            train_file_name,
            skip_window,
            sample_rate,
            queue_capacity,
            graph: tf.Graph):
        """
        Create data reader for road2vec
        :param train_file_name:
        :param skip_window:
        :param sample_rate:
        :param queue_capacity:
        """

        self.train_file_name = train_file_name
        self.skip_window = skip_window
        self.sample_rate = sample_rate

        self.graph = graph

        with self.graph.as_default():
            self.input_queue = tf.FIFOQueue(
                queue_capacity,
                dtypes=[tf.int32, tf.int32],
                shapes=[[], []],
                names=["inputs", "labels"],
                name="input_queue")
            self.enqueue_input = tf.placeholder(tf.int32, shape=[], name="enqueue_input")
            self.enqueue_label = tf.placeholder(tf.int32, shape=[], name="enqueue_label")
            self.enqueue_op = self.input_queue.enqueue(
                {
                    "inputs": self.enqueue_input,
                    "labels": self.enqueue_label
                },
                name="enqueue_op")

    def load_and_enqueue(self, sess: tf.Session):
        with open(self.train_file_name) as f:
            for line in f:
                traj = line2int_list(line)
                traj_len = len(traj)
                for input_id in range(traj_len):
                    l = max(0, input_id - self.skip_window)
                    r = min(traj_len, input_id + self.skip_window + 1)
                    for target_id in range(l, r):
                        if input_id != target_id:
                            if self.sample_rate >= 1 or random.random() < self.sample_rate:
                                sess.run(
                                    self.enqueue_op,
                                    feed_dict={
                                        self.enqueue_input: traj[input_id],
                                        self.enqueue_label: traj[target_id]
                                    })
