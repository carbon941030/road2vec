# -*- coding: utf-8 -*-

import math

import tensorflow as tf


class Road2VecModel(object):

    def __init__(
            self,
            batch_size,
            vocabulary_size,
            embedding_size,
            num_sampled,
            lr,
            valid_road_ids,
            graph: tf.Graph,
            global_step: tf.Variable,
            input_queue: tf.QueueBase):
        """
        Create model for road2vec
        :param batch_size:
        :param vocabulary_size:
        :param embedding_size:
        :param num_sampled:
        :param lr:
        :param valid_road_ids:
        :param input_queue:
        """

        self.graph = graph

        with self.graph.as_default():

            # Input data
            dequeue_dict = input_queue.dequeue_many(batch_size)
            inputs = dequeue_dict['inputs']
            labels = dequeue_dict['labels']

            # Embeddings
            self.embeddings = tf.Variable(
                tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0),
                name="embeddings")
            self.embed = tf.nn.embedding_lookup(self.embeddings, inputs)

            # parameters and loss
            self.weights = tf.Variable(
                tf.truncated_normal(
                    [vocabulary_size, embedding_size],
                    stddev=1.0 / math.sqrt(embedding_size)),
                name="weights")
            self.biases = tf.Variable(
                tf.zeros([vocabulary_size]),
                name="biases")

            if num_sampled > 0:
                self.loss = tf.reduce_mean(
                    tf.nn.nce_loss(
                        weights=self.weights,
                        biases=self.biases,
                        labels=tf.reshape(labels, [batch_size, 1]),
                        inputs=self.embed,
                        num_sampled=num_sampled,
                        num_classes=vocabulary_size),
                    name="nce_loss")
            else:
                logits = tf.add(tf.matmul(self.embed, self.weights, transpose_b=True), self.biases)
                losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=labels)
                self.loss = tf.reduce_mean(losses)
            tf.summary.scalar(name="nce_loss_summary", tensor=self.loss)

            # Optimizer
            self.train_op = tf.train.AdamOptimizer(learning_rate=lr).minimize(self.loss, global_step=global_step)

            # Normalize embeddings
            self.norm = tf.sqrt(tf.reduce_sum(tf.square(self.embeddings), 1, keep_dims=True))
            self.normalized_embeddings = tf.divide(self.embeddings, self.norm)

            # Validation during training
            if valid_road_ids is not None:
                self.valid_road_ids = tf.constant(valid_road_ids, dtype=tf.int32)
                self.valid_embeddings = tf.nn.embedding_lookup(self.normalized_embeddings, self.valid_road_ids)
                self.similarity = tf.matmul(self.valid_embeddings, self.normalized_embeddings, transpose_b=True)
