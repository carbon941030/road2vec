# -*- coding: utf-8 -*-

from collections import Counter

import random
import sys
import threading

import tensorflow as tf

from utils import line2int_list
from reader import Reader
from road2vec_model import Road2VecModel


class Road2VecTrainer(object):

    def __init__(
            self,
            train_filename,
            skip_window,
            sample_rate,
            queue_capacity,
            batch_size,
            vocabulary_size,
            embedding_size,
            num_sampled,
            lr,
            valid_window,
            valid_size,
            log_dir,
            stop_control_filename):
        """
        Creater trainer for road2vec
        :param train_filename:
        :param skip_window:
        :param sample_rate:
        :param queue_capacity:
        :param batch_size:
        :param vocabulary_size:
        :param embedding_size:
        :param num_sampled:
        :param lr:
        :param valid_window:
        :param valid_size:
        :param log_dir:
        :param stop_control_filename:
        """

        self.graph = tf.Graph()

        self.stop_control_filename = stop_control_filename

        self.reader = Reader(
            train_filename,
            skip_window,
            sample_rate,
            queue_capacity,
            self.graph)

        if valid_size > 0:
            road_counter = Counter()
            with open(train_filename, "r") as f:
                for line in f:
                    road_counter += Counter(line2int_list(line))
            most_popular_roads = dict(road_counter.most_common(valid_window))
            valid_road_ids = list(most_popular_roads.keys())
            random.shuffle(valid_road_ids)
            valid_road_ids = valid_road_ids[:valid_size]
        else:
            valid_road_ids = None

        with self.graph.as_default():
            self.global_step = tf.Variable(0, name='global_step', trainable=False)

        self.model = Road2VecModel(
            batch_size,
            vocabulary_size,
            embedding_size,
            num_sampled,
            lr,
            valid_road_ids,
            self.graph,
            self.global_step,
            self.reader.input_queue)

        self.sv = tf.train.Supervisor(
            logdir=log_dir,
            graph=self.graph,
            global_step=self.global_step,
            save_summaries_secs=30)

    @staticmethod
    def shoud_stop_by_outerfile(filename) -> bool:
        should_stop = False
        with open(filename, "r") as f:
            command = f.readline()
            if command == "STOP":
                should_stop = True
        return should_stop

    def keep_loading(self, should_stop, sess:tf.Session):
        epoc_count = 0
        while not should_stop():
            epoc_count += 1
            self.reader.load_and_enqueue(sess)
            print("Epoc %d load finished."%(epoc_count))
        self.reader.input_queue.close()

    def train(self):
        print('Start training.')
        with self.sv.managed_session() as sess:
            load_thread = threading.Thread(
                target=self.keep_loading,
                args=(lambda: Road2VecTrainer.shoud_stop_by_outerfile(self.stop_control_filename), sess))

            load_thread.start()

            while not self.sv.should_stop():
                _, step = sess.run([self.model.train_op, self.global_step])
                if step % 100 == 0:
                    print('.', end='', flush=True)
                    if step % 2000 == 0:
                        print('')
                    if step % 10000 == 0:
                        print('Training step %d finished.'%(step))