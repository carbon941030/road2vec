# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string('embeddings_loading_path', '',
                       'Path of embedding checkpoint.')
tf.flags.DEFINE_string('embeddings_dump_path', '',
                       'Path to dump embedding.')

tf.flags.DEFINE_integer('vocabulary_size', 0,
                        'Number of roads.')
tf.flags.DEFINE_integer('embedding_size', 0,
                        'Dimension of the embedding vector.')


def main(_):
    vocabulary_size = FLAGS.vocabulary_size
    embedding_size = FLAGS.embedding_size
    embeddings_loading_path = FLAGS.embeddings_loading_path
    embeddings_dump_path = FLAGS.embeddings_dump_path

    with tf.device('/cpu:0'):
        embeddings = tf.Variable(
            tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0),
            name="embeddings")
    embeddings_saver = tf.train.Saver({'embeddings': embeddings})
    with tf.Session() as sess:
        embeddings_saver.restore(sess, embeddings_loading_path)
        dumped_embeddings = sess.run(embeddings)
    np.savetxt(embeddings_dump_path, dumped_embeddings, delimiter=",")


if __name__ == '__main__':
    tf.app.run()