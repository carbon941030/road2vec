# -*- coding: utf-8 -*-


def line2int_list(line: str) -> list:
    line = line.split(',')
    line = map(lambda x: x.strip(), line)
    line = filter(lambda x: x != '', line)
    line = map(lambda x: int(x), line)
    return list(line)