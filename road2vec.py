#!/bin/python3
# -*- coding: utf-8 -*-

import tensorflow as tf

from road2vec_trainer import Road2VecTrainer

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string('train_filename', './cleaned_mm_edges.txt',
                       'Path and filename of the dataset.')
tf.flags.DEFINE_string('log_dir', './log/',
                       'Path of log data.')
tf.flags.DEFINE_string('stop_control_filename', './train_command.txt',
                       'Path of the file that control the training behavior(stop or continue).')

tf.flags.DEFINE_integer('skip_window', 5,
                        'Number of words to consider left and right.')
tf.flags.DEFINE_integer('queue_capacity', 1024,
                        'Capacity of input queue.')
tf.flags.DEFINE_integer('batch_size', 256,
                        'Size of each patch.')
tf.flags.DEFINE_integer('vocabulary_size', 10000,
                        'Number of roads.')
tf.flags.DEFINE_integer('embedding_size', 400,
                        'Dimension of the embedding vector.')
tf.flags.DEFINE_integer('num_sampled', 64,
                        'Number of negative examples to sample.')
tf.flags.DEFINE_integer('valid_window', 0,
                        'Number of samples in the head of distribution for validation.')
tf.flags.DEFINE_integer('valid_size', 0,
                        'Number of samples for validation.')

tf.flags.DEFINE_float('sample_rate', 0.5,
                      'Rate of target words from skip window for each center word to sample.')
tf.flags.DEFINE_float('lr', 0.03,
                      'Learning rate for training.')


def main(_):
    trainer = Road2VecTrainer(
        train_filename=FLAGS.train_filename,
        skip_window=FLAGS.skip_window,
        sample_rate=FLAGS.sample_rate,
        queue_capacity=FLAGS.queue_capacity,
        batch_size=FLAGS.batch_size,
        vocabulary_size=FLAGS.vocabulary_size,
        embedding_size=FLAGS.embedding_size,
        num_sampled=FLAGS.num_sampled,
        lr=FLAGS.lr,
        valid_window=FLAGS.valid_window,
        valid_size=FLAGS.valid_size,
        log_dir=FLAGS.log_dir,
        stop_control_filename=FLAGS.stop_control_filename)
    trainer.train()

if __name__ == '__main__':
    tf.app.run()